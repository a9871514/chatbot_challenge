from seoul_univ import seoul_univ
from korea_univ import korea_univ
from yonsei_univ import yonsei_univ

def haksik(first_statement, src, third_statement, fourth_statement):
    # 여기에 함수를 구현해봅시다.
    if first_statement == 1:
        return seoul_univ(src, third_statement, fourth_statement)

    elif first_statement == 2:
        return korea_univ(src, third_statement, fourth_statement)

    elif first_statement == 3:
        return yonsei_univ(src, third_statement, fourth_statement)