import urllib.request
from bs4 import BeautifulSoup
import datetime

def yonsei_univ(src, third_statement, fourth_statement):
    url = src
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    menu = []
    message = []
    card_title = []
    title_list = {' '}  # card-title 담을 set 선언
    dt = datetime.datetime.now().day

    for i in soup.find_all("div", class_="date-wrapper"):
        menu.append(i.get_text())
        for j in i.find_all("div", class_="card-title"):
            card_title.append(j.get_text())

    menu_list = []
    date_list = [' ', ' ', ' ', ' ', ' ', ' ', ' ']
    date_cnt = 0

    for i in soup.find_all("div", class_="date-title"):
        date_str = i.get_text().split('월 ')[1].split('일')[0]
        if str(dt) in date_str:
            date_list[date_cnt] = date_str
            dt = dt + 1
            date_cnt = date_cnt + 1
        else:
            while (str(dt) not in date_str) and (date_cnt < len(date_list)):
                dt = dt + 1
                date_cnt = date_cnt + 1

    # return date_list

    dt = datetime.datetime.now().day

    for j in range(len(date_list)):
        date_str = date_list[j]
        if str(dt) in date_str:
            menu_list.append(menu[j])
            dt = dt + 1
        else:
            # while str(dt) not in date_str:
            menu_list.append(' ')
            dt = dt + 1
        j = j + 1

    # title_list : card-title들의 목록 (중복 제거하고 키워드 하나씩만 남게)
    title_list.update(card_title)
    title_list.remove(' ')
    # print(title_list)

    for i in title_list:
        # menu_list의 원소를 str 형태로 total 변수에 삽입
        # third_statement = 1
        if third_statement <= len(menu_list):
            total = menu_list[third_statement - 1]

    # total 문자열에 '아침'이 있으면 temp 리스트에 '아침'삽입
    # 점심, 저녁도 마찬가지
    # 아침, 점심, 저녁이 total 문자열에 포함되어 있지 않으면 그 날에는 운영 x
    temp = []
    if '아침' in total:
        temp.append('아침')
    if '점심' in total:
        temp.append('점심')
    if '저녁' in total:
        temp.append('저녁')
    if '종일' in total:
        temp.append('종일')
    if '아침' not in total and '점심' not in total and '저녁' not in total and '종일' not in total:
        return ['이 날에는 운영하지 않습니다.']

    # for문을 이용해 temp 리스트의 원소 '아침', '점심', '저녁'을 기준으로 문자열을 나눔
    # 아침 뒤 ~ 점심 앞 까지의 문자열 / 점심 뒤 ~ 저녁 앞 까지의 문자열
    # 나눈 문자열을 total_menu 리스트의 원소로 삽입
    total_menu = []

    for i in range(len(temp)):
        if i < len(temp) - 1:
            total_menu.append(total.split(temp[i])[1].split(temp[i + 1])[0])
        if i == len(temp) - 1:
            total_menu.append(total.split(temp[i])[1])

    # 아점저 기본값: '없음'

    breakf = '아침: 없음'
    lunch = '점심: 없음'
    dinner = '저녁: 없음'
    jongil = '종일: 없음'

    # if 사용자가 '아침', '점심', '저녁' 입력:
    for i in range(len(temp)):
        if temp[i] == '아침':
            breakf = '<<' + temp[i] + '>>' + total_menu[i]
        if temp[i] == '점심':
            lunch = '\n<<' + temp[i] + '>>' + total_menu[i]
        if temp[i] == '저녁':
            dinner = '\n<<' + temp[i] + '>>' + total_menu[i]
        if temp[i] == '종일':
            jongil = '\n<<' + temp[i] + '>>' + total_menu[i]

    # title_list 집합을 이용하여 title_list의 원소 단위로 문자열을 나눔
    '''
    <<아침>>

    - [2500원] 집밥정식(조식)
    닭고구마고추장볶음
    (계육:브라질산)

    - [3500원] Hotbowl
    순두부찌개
    <<점심>>

    - [4300원] Hotbowl
    돈육김치찌개
    (돈육:국내산,사골엑기스-우사골:뉴질랜드산)

    - [4500원] Hotbowl
    설렁탕
    (우육:호주산,사골진육수-우육:호주산)

    - [5000원] Hotbowl
    차돌된장찌개
    (우육:미국산)
    '''
    for i in title_list:
        if i in breakf:
            breakf = breakf.replace(i, '\n\n- ' + i + '\n')
        if i in lunch:
            lunch = lunch.replace(i, '\n\n- ' + i + '\n')
        if i in dinner:
            dinner = dinner.replace(i, '\n\n- ' + i + '\n')
        if i in jongil:
            jongil = jongil.replace(i, '\n\n- ' + i + ' 메뉴\n')

    # for i in range(len(menu_list)):
    #     message.append(menu_list[i])
    # fourth_statement=4
    if fourth_statement == 1:
        message.append(breakf)
    elif fourth_statement == 2:
        message.append(lunch)
    elif fourth_statement == 3:
        message.append(dinner)
    elif fourth_statement == 4:
        message.append(breakf)
        message.append(lunch)
        message.append(dinner)
    message.append(jongil)

    return message