import urllib.request
from bs4 import BeautifulSoup
import datetime

def korea_univ(src, third_statement, fourth_statement):
    url = src
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    menu = []
    message = []
    dt = datetime.datetime.now().day

    for l in soup.find_all("div", class_="date-wrapper"):
        menu.append(l.get_text())  # 날짜별


    menu_list = []
    date_list = [' ', ' ', ' ', ' ', ' ', ' ', ' ']
    date_cnt = 0

    for i in soup.find_all("div", class_="date-title"):
        date_str = i.get_text().split('월 ')[1].split('일')[0]
        if str(dt) in date_str:
            date_list[date_cnt] = date_str
            dt = dt + 1
            date_cnt = date_cnt + 1
        else:
            while (str(dt) not in date_str) and (date_cnt < len(date_list)):
                dt = dt + 1
                date_cnt = date_cnt + 1

    # return date_list

    dt = datetime.datetime.now().day

    for j in range(len(date_list)):
        date_str = date_list[j]
        if str(dt) in date_str:
            menu_list.append(menu[j])
            dt = dt + 1
        else:
            # while str(dt) not in date_str:
            menu_list.append(' ')
            dt = dt + 1
        j = j+1

    # return [menu_list]

    # menu_list의 원소를 str 형태로 total 변수에 삽입
    # third_statement = 1
    if third_statement <= len(menu_list):
        total = menu_list[third_statement - 1]

    # total 문자열에 '아침'이 있으면 temp 리스트에 '아침'삽입
    # 점심, 저녁도 마찬가지
    # 아침, 점심, 저녁이 total 문자열에 포함되어 있지 않으면 그 날에는 운영 x
    temp = []
    if '아침' in total:
        temp.append('아침')
    if '점심' in total:
        temp.append('점심')
    if '저녁' in total:
        temp.append('저녁')
    if '종일' in total:
        temp.append('종일')
    if '아침' not in total and '점심' not in total and '저녁' not in total and '종일' not in total:
        return ['이 날에는 운영하지 않습니다.']

    # for문을 이용해 temp 리스트의 원소 '아침', '점심', '저녁'을 기준으로 문자열을 나눔
    # 아침 뒤 ~ 점심 앞 까지의 문자열 / 점심 뒤 ~ 저녁 앞 까지의 문자열
    # 나눈 문자열을 total_menu 리스트의 원소로 삽입
    total_menu = []

    for i in range(len(temp)):
        if i < len(temp) - 1:
            total_menu.append(total.split(temp[i])[1].split(temp[i + 1])[0])
        if i == len(temp) - 1:
            total_menu.append(total.split(temp[i])[1])

    # 아점저 기본값: '없음'

    breakf = '아침: 없음'
    lunch = '점심: 없음'
    dinner = '저녁: 없음'
    jongil = '종일: 없음'

    # if 사용자가 '아침', '점심', '저녁' 입력:
    for i in range(len(temp)):
        if temp[i] == '아침':
            breakf = temp[i] + ': ' + total_menu[i]
        if temp[i] == '점심':
            lunch = temp[i] + ': ' + total_menu[i]
        if temp[i] == '저녁':
            dinner = temp[i] + ': ' + total_menu[i]
        if temp[i] == '종일':
            jongil = temp[i] + ' 메뉴: ' + total_menu[i]

    if fourth_statement == 1:
        message.append(breakf)
    elif fourth_statement == 2:
        message.append(lunch)
    elif fourth_statement == 3:
        message.append(dinner)
    elif fourth_statement == 4:
        message.append(breakf)
        message.append(lunch)
        message.append(dinner)
    message.append(jongil)

    return message